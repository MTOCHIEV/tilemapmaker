package tilemapmaker.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import lombok.Data;
import tilemapmaker.data.Tile;
import tilemapmaker.service.TileMapMakerBean;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Data
@Named("mapStreamerBean")
@RequestScoped
public class MapStreamerBean {

    @Inject
    private TileMapMakerBean tileMapMakerBean;

    public static BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage resizedImage = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resizedImage.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resizedImage;
    }

    public byte[] getConvertedMap() {
        int mapWidthInTiles = tileMapMakerBean.getMapWidthInTiles();
        int mapHeightInTiles = tileMapMakerBean.getMapHeightInTiles();
        int tileSize = tileMapMakerBean.getTileSize();
        Tile[][] map = tileMapMakerBean.getMap();

        BufferedImage convertedMap = new BufferedImage(mapWidthInTiles * tileSize, mapHeightInTiles * tileSize, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = convertedMap.createGraphics();
        g2.setFont(new Font("Serif", Font.BOLD, Math.round(tileSize / 4)));
        g2.setColor(Color.RED);
        for (int tileColumn = 0; tileColumn < mapWidthInTiles; tileColumn++) {
            for (int tileRow = 0; tileRow < mapHeightInTiles; tileRow++) {
                BufferedImage resizedImage = resize(map[tileColumn][tileRow].getImage(), tileSize, tileSize);
                g2.drawImage(resizedImage, null, tileColumn * tileSize, tileRow * tileSize);
                if (tileMapMakerBean.isDebugView()) {
                    g2.setColor(Color.BLACK);
                    g2.drawRect(tileColumn * tileSize, tileRow * tileSize, tileSize, tileSize);
                    g2.setColor(Color.WHITE);
                    g2.drawString(String.valueOf(map[tileColumn][tileRow].getTileOptions().size()), tileColumn * tileSize + tileSize / 10, tileRow * tileSize + g2.getFont().getSize());
                    g2.drawString(map[tileColumn][tileRow].getXPosition() + "   " + map[tileColumn][tileRow].getYPosition(), tileColumn * tileSize + tileSize / 10, tileRow * tileSize + g2.getFont().getSize() + (tileSize / 3));
                }
            }
        }
        g2.dispose();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(convertedMap, "JPEG", baos);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }
}

