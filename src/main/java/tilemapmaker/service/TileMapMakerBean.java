package tilemapmaker.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.module.paranamer.ParanamerModule;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.component.UIInput;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.AjaxBehaviorEvent;
import jakarta.inject.Named;
import lombok.Data;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.file.UploadedFile;
import org.primefaces.shaded.commons.io.FilenameUtils;
import org.springframework.http.MediaType;
import tilemapmaker.data.Tile;
import tilemapmaker.data.TileDAO;
import tilemapmaker.data.TileSet;
import tilemapmaker.data.TileSetDAO;
import tilemapmaker.util.ImageUtils;
import tilemapmaker.util.TileMapMakerUtils;
import tilemapmaker.util.TileVariation;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;


@Data
@Named("tileMapMakerBean")
@SessionScoped
public class TileMapMakerBean {

    private Random random;
    private TileSet loadedTileSet;
    private String tileSetName;
    private TileSet emptyTileSet = TileSet.getEmptyTileSet();
    private Tile[][] map;

    private BufferedImage newTileImage;
    private String newTileName;
    private int newTileTopSocket;
    private int newTileRightSocket;
    private int newTileBottomSocket;
    private int newTileLeftSocket;
    private String newTileTopCanConnectTo;
    private String newTileRightCanConnectTo;
    private String newTileBottomCanConnectTo;
    private String newTileLeftCanConnectTo;
    private int newTileWeight;

    private int mapWidthInTiles;
    private int mapHeightInTiles;
    private int tileSize;

    private boolean debugView;
    private boolean setMapSizeButtonDisabled;
    private boolean resetButtonDisabled;
    private boolean randomizeButtonDisabled;
    private boolean nextStepButtonDisabled;
    private boolean finishMapButtonDisabled;
    private boolean addConfigureMapWidthButtonDisabled;
    private boolean removeConfigureMapWidthButtonDisabled;
    private boolean addConfigureMapHeightButtonDisabled;
    private boolean removeConfigureMapHeightButtonDisabled;

    private Tile[][] configureMap;
    private int configureMapWidth;
    private int configureMapHeight;
    private int currentConfigureMapRow;
    private int currentConfigureMapColumn;
    private UploadedFile file;

    @PostConstruct
    public void init() {
        this.random = new Random();
        this.setMapSizeButtonDisabled = true;
        this.resetButtonDisabled = true;
        this.randomizeButtonDisabled = true;
        this.nextStepButtonDisabled = true;
        this.finishMapButtonDisabled = true;
        this.mapWidthInTiles = 20;
        this.mapHeightInTiles = 20;
        this.tileSize = 32;
        loadDefaultConfigureMap();
    }

    public void loadDefaultTileset() {
        this.loadedTileSet = TileSet.getDefaultTileSet();
        this.setMapSizeButtonDisabled = false;
        updateConfigureMapWithTileset(loadedTileSet);
    }

    public void setMapSize() {
        this.map = getDefaultMap(mapWidthInTiles, mapHeightInTiles);
        this.finishMapButtonDisabled = true;
        this.resetButtonDisabled = false;
        this.nextStepButtonDisabled = false;
        this.randomizeButtonDisabled = false;
    }

    public void resetMap() {
        this.map = getDefaultMap(mapWidthInTiles, mapHeightInTiles);
        this.finishMapButtonDisabled = true;
        this.nextStepButtonDisabled = false;
    }

    public void resetConfigureMap() {
        this.loadedTileSet = null;
        this.tileSetName = null;
        this.setMapSizeButtonDisabled = true;
        this.resetButtonDisabled = true;
        this.randomizeButtonDisabled = true;
        this.nextStepButtonDisabled = true;
        this.finishMapButtonDisabled = true;
        this.configureMapHeight = 4;
        this.configureMapWidth = 4;
        this.configureMap = new Tile[configureMapHeight][configureMapWidth];
    }

    public void randomizeMap() {
        this.map = getDefaultMap(mapWidthInTiles, mapHeightInTiles);
        while (isMapNotCollapsed()) {
            Tile lowestEntropyTile = chooseLowestEntropyTile();
            Tile chosenTile = chooseOptionForLowestEntropyTile(lowestEntropyTile);
            propagateToNeighbours(chosenTile);
        }

        this.finishMapButtonDisabled = true;
        this.nextStepButtonDisabled = true;
    }

    public void nextStep() {
        Tile lowestEntropyTile = chooseLowestEntropyTile();
        Tile chosenOptionForLowestEntropyTile = chooseOptionForLowestEntropyTile(lowestEntropyTile);
        propagateToNeighbours(chosenOptionForLowestEntropyTile);

        this.finishMapButtonDisabled = false;
        if (!isMapNotCollapsed()) {
            this.finishMapButtonDisabled = true;
            this.nextStepButtonDisabled = true;
        }
    }

    public void finishMap() {
        while (isMapNotCollapsed()) {
            Tile lowestEntropyTile = chooseLowestEntropyTile();
            Tile chosenTile = chooseOptionForLowestEntropyTile(lowestEntropyTile);
            propagateToNeighbours(chosenTile);
        }
        this.finishMapButtonDisabled = true;
        this.nextStepButtonDisabled = true;
    }

    private Tile chooseLowestEntropyTile() {
        int lowestNumberOfOptions = Integer.MAX_VALUE;
        List<Tile> listOfPossibleTiles = new ArrayList<>();
        for (int tileColumn = 0; tileColumn < this.mapWidthInTiles; tileColumn++) {
            for (int tileRow = 0; tileRow < this.mapHeightInTiles; tileRow++) {
                if (map[tileColumn][tileRow].isCollapsed()) {
                } else if (map[tileColumn][tileRow].getTileOptions().size() < lowestNumberOfOptions) {
                    lowestNumberOfOptions = map[tileColumn][tileRow].getTileOptions().size();
                    listOfPossibleTiles = new ArrayList<>();
                    listOfPossibleTiles.add(map[tileColumn][tileRow]);
                } else if (map[tileColumn][tileRow].getTileOptions().size() == lowestNumberOfOptions) {
                    listOfPossibleTiles.add(map[tileColumn][tileRow]);
                }
            }
        }
        if (listOfPossibleTiles.size() > 1) {
            return listOfPossibleTiles.get(random.nextInt(listOfPossibleTiles.size()));
        } else if (listOfPossibleTiles.size() == 1) {
            return listOfPossibleTiles.get(0);
        } else {
            return null;
        }
    }

    private Tile chooseOptionForLowestEntropyTile(Tile lowestEntropyTile) {
        if (lowestEntropyTile == null) return null;
        int x = lowestEntropyTile.getXPosition();
        int y = lowestEntropyTile.getYPosition();
        List<Tile> weightedTileList = new ArrayList<>();
        for (Tile tile : lowestEntropyTile.getTileOptions()) {
            for (int i = 0; i < tile.getWeight(); i++) {
                weightedTileList.add(tile);
            }
        }
        Collections.shuffle(weightedTileList);
        if (weightedTileList.size() == 0) {
            lowestEntropyTile = emptyTileSet.getGeneratedTileOptions().get(0);
        } else {
            lowestEntropyTile = weightedTileList.get(0);
        }
        lowestEntropyTile.setXPosition(x);
        lowestEntropyTile.setYPosition(y);
        map[x][y] = lowestEntropyTile;
        map[x][y].setTileOptions(new ArrayList<>());
        map[x][y].setCollapsed(true);
        return lowestEntropyTile;
    }

    private void propagateToNeighbours(Tile chosenTile) {
        if (chosenTile == null) return;
        if (chosenTile.getXPosition() - 1 >= 0) {
            List<Tile> possibleLeftNeighbours = map[chosenTile.getXPosition() - 1][chosenTile.getYPosition()].getTileOptions();
            List<Tile> newListOfPossibleNeighbours = new ArrayList<>();
            for (Tile neighBourTile : possibleLeftNeighbours) {
                if (ArrayUtils.contains(neighBourTile.getRightCanConnectTo(), chosenTile.getLeftSocket())) {
                    newListOfPossibleNeighbours.add(neighBourTile);
                }
            }
            map[chosenTile.getXPosition() - 1][chosenTile.getYPosition()].setTileOptions(newListOfPossibleNeighbours);
        }
        if (chosenTile.getXPosition() + 1 < mapWidthInTiles) {
            List<Tile> possibleRightNeighbours = map[chosenTile.getXPosition() + 1][chosenTile.getYPosition()].getTileOptions();
            List<Tile> newListOfPossibleNeighbours = new ArrayList<>();
            for (Tile neighBourTile : possibleRightNeighbours) {
                if (ArrayUtils.contains(neighBourTile.getLeftCanConnectTo(), chosenTile.getRightSocket())) {
                    newListOfPossibleNeighbours.add(neighBourTile);
                }
            }
            map[chosenTile.getXPosition() + 1][chosenTile.getYPosition()].setTileOptions(newListOfPossibleNeighbours);
        }
        if (chosenTile.getYPosition() - 1 >= 0) {
            List<Tile> possibleTopNeighbours = map[chosenTile.getXPosition()][chosenTile.getYPosition() - 1].getTileOptions();
            List<Tile> newListOfPossibleNeighbours = new ArrayList<>();
            for (Tile neighBourTile : possibleTopNeighbours) {
                if (ArrayUtils.contains(neighBourTile.getBottomCanConnectTo(), chosenTile.getTopSocket())) {
                    newListOfPossibleNeighbours.add(neighBourTile);
                }
            }
            map[chosenTile.getXPosition()][chosenTile.getYPosition() - 1].setTileOptions(newListOfPossibleNeighbours);
        }
        if (chosenTile.getYPosition() + 1 < mapHeightInTiles) {
            List<Tile> possibleBottomNeighbours = map[chosenTile.getXPosition()][chosenTile.getYPosition() + 1].getTileOptions();
            List<Tile> newListOfPossibleNeighbours = new ArrayList<>();
            for (Tile neighBourTile : possibleBottomNeighbours) {
                if (ArrayUtils.contains(neighBourTile.getTopCanConnectTo(), chosenTile.getBottomSocket())) {
                    newListOfPossibleNeighbours.add(neighBourTile);
                }
            }
            map[chosenTile.getXPosition()][chosenTile.getYPosition() + 1].setTileOptions(newListOfPossibleNeighbours);
        }

    }

    private boolean isMapNotCollapsed() {
        for (int tileColumn = 0; tileColumn < mapWidthInTiles; tileColumn++) {
            for (int tileRow = 0; tileRow < mapHeightInTiles; tileRow++) {
                if (!this.map[tileColumn][tileRow].isCollapsed()) {
                    return true;
                }
            }
        }
        return false;
    }

    public Tile[][] getDefaultMap(int width, int height) {
        Tile[][] defaultMap = new Tile[width][height];
        for (int tileColumn = 0; tileColumn < width; tileColumn++) {
            for (int tileRow = 0; tileRow < height; tileRow++) {
                defaultMap[tileColumn][tileRow] = new Tile(this.emptyTileSet.getGeneratedTileOptions().get(0), TileVariation.ROT0);
                defaultMap[tileColumn][tileRow].setTileOptions(this.loadedTileSet.getGeneratedTileOptions());
                defaultMap[tileColumn][tileRow].setXPosition(tileColumn);
                defaultMap[tileColumn][tileRow].setYPosition(tileRow);
                defaultMap[tileColumn][tileRow].setCollapsed(false);
            }
        }
        return defaultMap;
    }

    public void addMessage(AjaxBehaviorEvent event) {
        UIComponent component = event.getComponent();
        if (component instanceof UIInput inputComponent) {
            Boolean value = (Boolean) inputComponent.getValue();
            String summary = value ? "Debug View enabled" : "Debuge View disabled";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));
        }
    }

    private void loadDefaultConfigureMap() {
        configureMapWidth = 4;
        configureMapHeight = 4;
        this.configureMap = new Tile[configureMapHeight][configureMapWidth];
    }

    public void updateConfigureMapWithTileset(TileSet tileSet) {
        this.tileSetName = tileSet.getName();
        int nearestBiggerSquareRoot = (int) Math.ceil(Math.sqrt(tileSet.getTileSheet().size()));
        this.configureMapHeight = this.configureMapWidth = nearestBiggerSquareRoot;
        this.configureMap = new Tile[nearestBiggerSquareRoot][nearestBiggerSquareRoot];
        int tileIterator = 0;
        for (Tile tile : tileSet.getTileSheet()) {
            int tileColumn = tileIterator % nearestBiggerSquareRoot;
            int tileRow = tileIterator / nearestBiggerSquareRoot;
            this.configureMap[tileRow][tileColumn] = tile;
            tileIterator++;
        }

    }

    public void updateConfigureMapWidth(boolean moreOrLessWidth) {
        Tile[][] oldConfigureMap = this.configureMap;
        int oldConfigureMapWidth = this.configureMapWidth;

        if (moreOrLessWidth) this.configureMapWidth++;
        else this.configureMapWidth--;

        this.configureMap = new Tile[configureMapHeight][configureMapWidth];
        for (int column = 0; column < configureMapWidth; column++) {
            for (int row = 0; row < configureMapHeight; row++) {
                if (column < oldConfigureMapWidth) this.configureMap[row][column] = oldConfigureMap[row][column];
            }
        }

        this.addConfigureMapWidthButtonDisabled = configureMapWidth > 9;
        this.removeConfigureMapWidthButtonDisabled = configureMapWidth < 2;
    }

    public void updateConfigureMapHeight(boolean moreOrLessHeight) {
        Tile[][] oldConfigureMap = this.configureMap;
        int oldConfigureMapHeight = this.configureMapHeight;

        if (moreOrLessHeight) this.configureMapHeight++;
        else this.configureMapHeight--;

        this.configureMap = new Tile[configureMapHeight][configureMapWidth];
        for (int column = 0; column < configureMapWidth; column++) {
            for (int row = 0; row < configureMapHeight; row++) {
                if (row < oldConfigureMapHeight) this.configureMap[row][column] = oldConfigureMap[row][column];
            }
        }

        this.addConfigureMapHeightButtonDisabled = configureMapHeight > 9;
        this.removeConfigureMapHeightButtonDisabled = configureMapHeight < 2;
    }

    public StreamedContent exportTileSetAsJSON() throws IOException {
        TileSetDAO tileSetDAO;
        if (this.loadedTileSet == null) tileSetDAO = TileSetDAO.getEmptyTileDAO();
        else tileSetDAO = new TileSetDAO(this.loadedTileSet);
        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = objectWriter.writeValueAsString(tileSetDAO);
        InputStream inputStream = new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8));
        return new DefaultStreamedContent().builder().name(tileSetDAO.getName()).contentType(MediaType.APPLICATION_JSON.toString()).stream(() -> inputStream).build();
    }

    public boolean checkIsTileEmpty(int y, int x) {
        return this.configureMap[y][x] != null;
    }


    public void resetNewTile(int rowIndex, int columnIndex) {
        newTileImage = null;
        newTileName = null;
        newTileTopSocket = 0;
        newTileRightSocket = 0;
        newTileBottomSocket = 0;
        newTileLeftSocket = 0;
        newTileTopCanConnectTo = null;
        newTileRightCanConnectTo = null;
        newTileBottomCanConnectTo = null;
        newTileLeftCanConnectTo = null;
        newTileWeight = 0;
        this.currentConfigureMapRow = rowIndex;
        this.currentConfigureMapColumn = columnIndex;
    }

    public void getConfigureMapTileInfo(int rowIndex, int columnIndex) {
        newTileImage = configureMap[rowIndex][columnIndex].getImage();
        newTileName = configureMap[rowIndex][columnIndex].getName();
        newTileTopSocket = configureMap[rowIndex][columnIndex].getTopSocket();
        newTileRightSocket = configureMap[rowIndex][columnIndex].getRightSocket();
        newTileBottomSocket = configureMap[rowIndex][columnIndex].getBottomSocket();
        newTileLeftSocket = configureMap[rowIndex][columnIndex].getLeftSocket();
        newTileTopCanConnectTo = TileMapMakerUtils.convertIntArrayToString(configureMap[rowIndex][columnIndex].getTopCanConnectTo());
        newTileRightCanConnectTo = TileMapMakerUtils.convertIntArrayToString(configureMap[rowIndex][columnIndex].getRightCanConnectTo());
        newTileBottomCanConnectTo = TileMapMakerUtils.convertIntArrayToString(configureMap[rowIndex][columnIndex].getBottomCanConnectTo());
        newTileLeftCanConnectTo = TileMapMakerUtils.convertIntArrayToString(configureMap[rowIndex][columnIndex].getLeftCanConnectTo());
        newTileWeight = configureMap[rowIndex][columnIndex].getWeight();
        this.currentConfigureMapRow = rowIndex;
        this.currentConfigureMapColumn = columnIndex;
    }

    public void deleteTileFromConfigureMap(int rowIndex, int columnIndex) {
        this.currentConfigureMapRow = rowIndex;
        this.currentConfigureMapColumn = columnIndex;
        configureMap[rowIndex][columnIndex] = null;
    }

    public void handleFileUpload(FileUploadEvent event) {
        System.out.println("TEST");
        UploadedFile uploadedFile = event.getFile();
        try {
            this.newTileImage = ImageIO.read(uploadedFile.getInputStream());
        } catch (Exception e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "File is not an image");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public String getBase64FromImage() {
        return Tile.convertImageToBase64(this.newTileImage);
    }

    public void addTileToConfigureMap() {
        if (StringUtils.isEmpty(this.newTileName)) {
            TileMapMakerUtils.addFacesMessage(FacesMessage.SEVERITY_ERROR, "Failed!", "Tile Name is empty");
        } else if (this.newTileWeight == 0) {
            TileMapMakerUtils.addFacesMessage(FacesMessage.SEVERITY_ERROR, "Failed!", "Tile weight cannot be 0");
        } else if (this.newTileImage == null) {
            TileMapMakerUtils.addFacesMessage(FacesMessage.SEVERITY_ERROR, "Failed!", "Tile Image is empty");
        } else if (StringUtils.isEmpty(this.newTileTopCanConnectTo) || StringUtils.isEmpty(this.newTileRightCanConnectTo) || StringUtils.isEmpty(this.newTileBottomCanConnectTo) || StringUtils.isEmpty(this.newTileLeftCanConnectTo)) {
            TileMapMakerUtils.addFacesMessage(FacesMessage.SEVERITY_ERROR, "Failed!", "Connectable edge-types is empty");
        } else if (!TileMapMakerUtils.isCommaSeparatedIntegerList(this.newTileTopCanConnectTo) || !TileMapMakerUtils.isCommaSeparatedIntegerList(this.newTileRightCanConnectTo) || !TileMapMakerUtils.isCommaSeparatedIntegerList(this.newTileBottomCanConnectTo) || !TileMapMakerUtils.isCommaSeparatedIntegerList(this.newTileLeftCanConnectTo)) {
            TileMapMakerUtils.addFacesMessage(FacesMessage.SEVERITY_ERROR, "Failed!", "Connectable edge-types is not a numbers only, comma separated list");
        } else if (this.newTileTopSocket == 0 || this.newTileRightSocket == 0 || this.newTileBottomSocket == 0 || this.newTileLeftSocket == 0) {
            TileMapMakerUtils.addFacesMessage(FacesMessage.SEVERITY_ERROR, "Failed!", "Edge type may not be 0");
        } else {
            int[] topCanConnectTo = TileMapMakerUtils.convertIntCSVToArray(this.newTileTopCanConnectTo);
            int[] rightCanConnectTo = TileMapMakerUtils.convertIntCSVToArray(this.newTileRightCanConnectTo);
            int[] bottomCanConnectTo = TileMapMakerUtils.convertIntCSVToArray(this.newTileBottomCanConnectTo);
            int[] leftCanConnectTo = TileMapMakerUtils.convertIntCSVToArray(this.newTileLeftCanConnectTo);
            Tile newTile = new Tile(this.newTileName, this.newTileImage, this.newTileWeight, this.newTileTopSocket, topCanConnectTo, this.newTileRightSocket, rightCanConnectTo, this.newTileBottomSocket, bottomCanConnectTo, this.newTileLeftSocket, leftCanConnectTo);
            this.configureMap[currentConfigureMapRow][currentConfigureMapColumn] = newTile;
        }
    }

    public void convertConfigureMapToTileSet() {
        TileSet tileSet = new TileSet();
        tileSet.setTileSheet(new ArrayList<>());
        tileSet.setName(this.tileSetName);
        for (int column = 0; column < configureMapWidth; column++) {
            for (int row = 0; row < configureMapHeight; row++) {
                if (configureMap[row][column] != null) {
                    tileSet.getTileSheet().add(configureMap[row][column]);
                }
            }
        }
        tileSet.setGeneratedTileOptions(TileSet.convertTileSheetToTileOptions(tileSet.getTileSheet()));
        this.loadedTileSet = tileSet;
        this.setMapSizeButtonDisabled = false;
    }

    public void importTileSet(FileUploadEvent event) {
        UploadedFile file = event.getFile();
        if (!file.getContentType().equalsIgnoreCase("application/json")) {
            TileMapMakerUtils.addFacesMessage(FacesMessage.SEVERITY_ERROR, "Failed!", "File was not JSON format");
        } else {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new ParanamerModule());
            TileSetDAO tileSetDAO = null;
            try {
                tileSetDAO = objectMapper.readValue(file.getContent(), TileSetDAO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

            TileSet tileSet = new TileSet();
            tileSet.setTileSheet(new ArrayList<>());

            for (TileDAO tileDAO : tileSetDAO.getTileSheet()) {
                Tile newTile = new Tile(tileDAO.getName(), ImageUtils.convertBase64StringToBufferedImage(tileDAO.getImageBase64()), tileDAO.getWeight(), tileDAO.getTopSocket(), tileDAO.getTopCanConnectTo(), tileDAO.getRightSocket(), tileDAO.getRightCanConnectTo(), tileDAO.getBottomSocket(), tileDAO.getBottomCanConnectTo(), tileDAO.getLeftSocket(), tileDAO.getLeftCanConnectTo());
                tileSet.getTileSheet().add(newTile);
            }
            tileSet.setGeneratedTileOptions(tileSet.getTileSheet());
            tileSet.setName(FilenameUtils.removeExtension(tileSetDAO.getName()));
            this.loadedTileSet = tileSet;
            updateConfigureMapWithTileset(this.loadedTileSet);
        }
    }

    public StreamedContent getDocumentationPDF() throws IOException {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("docs/TileMapMaker.pdf");
        return DefaultStreamedContent.builder().contentType("application/pdf").name("documentation").stream(() -> inputStream).build();
    }
}
