package tilemapmaker.util;

public class TileVariation {

    public static final int ROT0 = 0;
    public static final int ROT90 = 1;
    public static final int ROT180 = 2;
    public static final int ROT270 = 3;

    public static final int DEFAULT_EDGE = 0;
    public static final int GRASS_EDGE = 1;
    public static final int WATER_EDGE = 2;
    public static final int GRASS_WATER_EDGE = 3;
    public static final int WATER_GRASS_EDGE = 4;
    public static final int SAND_EDGE = 5;
    public static final int SAND_WATER_EDGE = 6;
    public static final int WATER_SAND_EDGE = 7;
    public static final int SAND_GRASS_EDGE = 8;
    public static final int GRASS_SAND_EDGE = 9;
}
