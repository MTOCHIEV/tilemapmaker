package tilemapmaker.util;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import org.springframework.stereotype.Component;

@Component
public class TileMapMakerUtils {

    public static void addFacesMessage(FacesMessage.Severity type, String summary, String details) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        FacesMessage message = new FacesMessage(type, summary, details);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public static boolean isCommaSeparatedIntegerList(String list) {
        for (char character : list.toCharArray()) {
            if (!Character.isDigit(character) && character != ',' && character != ' ') return false;
        }
        return true;
    }

    public static int[] convertIntCSVToArray(String list) {
        list = list.replaceAll("\\s+", "");
        String[] integerStrings = list.split(",");
        int[] result = new int[integerStrings.length];

        for (int i = 0; i < integerStrings.length; i++) {
            try {
                result[i] = Integer.parseInt(integerStrings[i].trim());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static String convertIntArrayToString(int[] array) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int a : array) {
            stringBuilder.append(a + ",");
        }
        stringBuilder.delete(stringBuilder.length() - 1, stringBuilder.length());
        return stringBuilder.toString();
    }
}
