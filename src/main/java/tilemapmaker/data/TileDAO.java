package tilemapmaker.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TileDAO {

    @JsonProperty("name")
    private final String name;
    @JsonProperty("id")
    private final String id;
    @JsonProperty("topSocket")
    private final int topSocket;
    @JsonProperty("rightSocket")
    private final int rightSocket;
    @JsonProperty("bottomSocket")
    private final int bottomSocket;
    @JsonProperty("leftSocket")
    private final int leftSocket;
    @JsonProperty("topCanConnectTo")
    private final int[] topCanConnectTo;
    @JsonProperty("rightCanConnectTo")
    private final int[] rightCanConnectTo;
    @JsonProperty("bottomCanConnectTo")
    private final int[] bottomCanConnectTo;
    @JsonProperty("leftCanConnectTo")
    private final int[] leftCanConnectTo;
    @JsonProperty("weight")
    private final int weight;
    @JsonProperty("imageBase64")
    private final String imageBase64;

    public TileDAO(String name, String id, int topSocket, int rightSocket, int bottomSocket, int leftSocket, int[] topCanConnectTo, int[] rightCanConnectTo, int[] bottomCanConnectTo, int[] leftCanConnectTo, int weight, String imageBase64) {
        this.name = name;
        this.id = id;
        this.topSocket = topSocket;
        this.rightSocket = rightSocket;
        this.bottomSocket = bottomSocket;
        this.leftSocket = leftSocket;
        this.topCanConnectTo = topCanConnectTo;
        this.rightCanConnectTo = rightCanConnectTo;
        this.bottomCanConnectTo = bottomCanConnectTo;
        this.leftCanConnectTo = leftCanConnectTo;
        this.weight = weight;
        this.imageBase64 = imageBase64;
    }

    public TileDAO(Tile tile) {
        this.name = tile.getName();
        this.id = tile.getId();
        this.topSocket = tile.getTopSocket();
        this.rightSocket = tile.getRightSocket();
        this.bottomSocket = tile.getBottomSocket();
        this.leftSocket = tile.getLeftSocket();
        this.topCanConnectTo = tile.getTopCanConnectTo();
        this.rightCanConnectTo = tile.getRightCanConnectTo();
        this.bottomCanConnectTo = tile.getBottomCanConnectTo();
        this.leftCanConnectTo = tile.getLeftCanConnectTo();
        this.weight = tile.getWeight();
        this.imageBase64 = tile.getImageBase64();
    }
}
