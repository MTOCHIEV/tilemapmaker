package tilemapmaker.data;

import lombok.Data;
import org.apache.tomcat.util.codec.binary.Base64;
import tilemapmaker.util.ImageUtils;
import tilemapmaker.util.TileVariation;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

@Data
public class Tile {
    private String name;
    private String id;
    private int xPosition;
    private int yPosition;
    private String pathToImage;
    private BufferedImage image;
    private int topSocket;
    private int rightSocket;
    private int bottomSocket;
    private int leftSocket;
    private int[] topCanConnectTo;
    private int[] rightCanConnectTo;
    private int[] bottomCanConnectTo;
    private int[] leftCanConnectTo;
    private boolean collapsed;
    private List<Tile> tileOptions;
    private int weight;
    private String imageBase64;

    public Tile(Tile tile, int variation) {
        this.name = tile.name;
        this.id = tile.getId();
        this.weight = tile.weight;
        this.pathToImage = tile.getPathToImage();
        this.collapsed = false;

        if (variation == TileVariation.ROT0) {
            this.image = ImageUtils.rotateImage(tile.image, 0);
            this.topSocket = tile.topSocket;
            this.rightSocket = tile.rightSocket;
            this.bottomSocket = tile.bottomSocket;
            this.leftSocket = tile.leftSocket;
            this.topCanConnectTo = tile.topCanConnectTo;
            this.rightCanConnectTo = tile.rightCanConnectTo;
            this.bottomCanConnectTo = tile.bottomCanConnectTo;
            this.leftCanConnectTo = tile.leftCanConnectTo;
        } else if (variation == TileVariation.ROT90) {
            this.image = ImageUtils.rotateImage(tile.image, 90);
            this.topSocket = tile.leftSocket;
            this.rightSocket = tile.topSocket;
            this.bottomSocket = tile.rightSocket;
            this.leftSocket = tile.bottomSocket;
            this.topCanConnectTo = tile.leftCanConnectTo;
            this.rightCanConnectTo = tile.topCanConnectTo;
            this.bottomCanConnectTo = tile.rightCanConnectTo;
            this.leftCanConnectTo = tile.bottomCanConnectTo;
        } else if (variation == TileVariation.ROT180) {
            this.image = ImageUtils.rotateImage(tile.image, 180);
            this.topSocket = tile.bottomSocket;
            this.rightSocket = tile.leftSocket;
            this.bottomSocket = tile.topSocket;
            this.leftSocket = tile.rightSocket;
            this.topCanConnectTo = tile.bottomCanConnectTo;
            this.rightCanConnectTo = tile.leftCanConnectTo;
            this.bottomCanConnectTo = tile.topCanConnectTo;
            this.leftCanConnectTo = tile.rightCanConnectTo;
        } else if (variation == TileVariation.ROT270) {
            this.image = ImageUtils.rotateImage(tile.image, 270);
            this.topSocket = tile.rightSocket;
            this.rightSocket = tile.bottomSocket;
            this.bottomSocket = tile.leftSocket;
            this.leftSocket = tile.topSocket;
            this.topCanConnectTo = tile.rightCanConnectTo;
            this.rightCanConnectTo = tile.bottomCanConnectTo;
            this.bottomCanConnectTo = tile.leftCanConnectTo;
            this.leftCanConnectTo = tile.topCanConnectTo;
        }
        this.imageBase64 = convertImageToBase64(this.image);
    }

    public Tile(String name, String pathToImage, int weight, int top, int[] topCanConnectTo, int right, int[] rightCanConnectTo, int bottom, int[] bottomCanConnectTo, int left, int[] leftCanConnectTo) {
        this.name = name;
        this.weight = weight;
        this.id = UUID.randomUUID().toString();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(pathToImage);
        try {
            this.image = ImageIO.read(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        this.image = ImageUtils.rotateImage(this.image, 0);
        this.topSocket = top;
        this.rightSocket = right;
        this.bottomSocket = bottom;
        this.leftSocket = left;
        this.topCanConnectTo = topCanConnectTo;
        this.rightCanConnectTo = rightCanConnectTo;
        this.bottomCanConnectTo = bottomCanConnectTo;
        this.leftCanConnectTo = leftCanConnectTo;
        this.imageBase64 = convertImageToBase64(this.image);
    }

    public Tile(String name, BufferedImage image, int weight, int top, int[] topCanConnectTo, int right, int[] rightCanConnectTo, int bottom, int[] bottomCanConnectTo, int left, int[] leftCanConnectTo) {
        this.name = name;
        this.weight = weight;
        this.id = UUID.randomUUID().toString();
        this.image = image;
        this.image = ImageUtils.rotateImage(this.image, 0);
        this.topSocket = top;
        this.rightSocket = right;
        this.bottomSocket = bottom;
        this.leftSocket = left;
        this.topCanConnectTo = topCanConnectTo;
        this.rightCanConnectTo = rightCanConnectTo;
        this.bottomCanConnectTo = bottomCanConnectTo;
        this.leftCanConnectTo = leftCanConnectTo;
        this.imageBase64 = convertImageToBase64(this.image);
    }

    public static String convertImageToBase64(BufferedImage image) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, "PNG", out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] bytes = out.toByteArray();
        String base64bytes = Base64.encodeBase64String(bytes);
        return "data:image/png;base64, " + base64bytes;
    }
}
