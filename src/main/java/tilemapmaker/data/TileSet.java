package tilemapmaker.data;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

import static tilemapmaker.util.TileVariation.*;

@Data
public class TileSet {

    private static TileSet defaultTileSet;
    private static TileSet getEmptyTileSet;
    private List<Tile> tileSheet;
    private List<Tile> generatedTileOptions;

    private String name;

    public TileSet() {
    }

    public static List<Tile> convertTileSheetToTileOptions(List<Tile> tileSheet) {
        List<Tile> generatedTileOptions = new ArrayList<>();
        for (Tile tile : tileSheet) {
            if (areAllEdgesEqual(tile.getTopSocket(), tile.getRightSocket(), tile.getBottomSocket(), tile.getLeftSocket())) {
                Tile variation1 = new Tile(tile, ROT0);
                generatedTileOptions.add(variation1);
            } else {
                Tile variation1 = new Tile(tile, ROT0);
                Tile variation2 = new Tile(tile, ROT90);
                Tile variation3 = new Tile(tile, ROT180);
                Tile variation4 = new Tile(tile, ROT270);
                generatedTileOptions.add(variation1);
                generatedTileOptions.add(variation2);
                generatedTileOptions.add(variation3);
                generatedTileOptions.add(variation4);
            }
        }
        return generatedTileOptions;
    }

    public static TileSet getDefaultTileSet() {
        TileSet defaultTileSet = new TileSet();
        defaultTileSet.setName("defaultTileSet");
        defaultTileSet.tileSheet = new ArrayList<>();
        Tile tileGrass = new Tile("tileGrass", "images/tilesets/01-grass.jpg", 10, GRASS_EDGE, new int[]{GRASS_EDGE}, GRASS_EDGE, new int[]{GRASS_EDGE}, GRASS_EDGE, new int[]{GRASS_EDGE}, GRASS_EDGE, new int[]{GRASS_EDGE});
        Tile tileGrass2 = new Tile("tileGrassBrushes", "images/tilesets/01b-grass.png", 5, GRASS_EDGE, new int[]{GRASS_EDGE}, GRASS_EDGE, new int[]{GRASS_EDGE}, GRASS_EDGE, new int[]{GRASS_EDGE}, GRASS_EDGE, new int[]{GRASS_EDGE});
        Tile tileWater = new Tile("tileWater", "images/tilesets/02-water.jpg", 3, WATER_EDGE, new int[]{WATER_EDGE}, WATER_EDGE, new int[]{WATER_EDGE}, WATER_EDGE, new int[]{WATER_EDGE}, WATER_EDGE, new int[]{WATER_EDGE});
        Tile tileWater2 = new Tile("tileWaterDeep", "images/tilesets/02b-water.png", 2, WATER_EDGE, new int[]{WATER_EDGE}, WATER_EDGE, new int[]{WATER_EDGE}, WATER_EDGE, new int[]{WATER_EDGE}, WATER_EDGE, new int[]{WATER_EDGE});
        Tile tileSand = new Tile("tileSand", "images/tilesets/03-sand.png", 1, SAND_EDGE, new int[]{SAND_EDGE}, SAND_EDGE, new int[]{SAND_EDGE}, SAND_EDGE, new int[]{SAND_EDGE}, SAND_EDGE, new int[]{SAND_EDGE});
        Tile tileWaterSandHalf = new Tile("tileSandWaterHalf", "images/tilesets/07-sandWaterHalf.png", 1, WATER_EDGE, new int[]{WATER_EDGE}, WATER_SAND_EDGE, new int[]{SAND_WATER_EDGE}, SAND_EDGE, new int[]{SAND_EDGE}, SAND_WATER_EDGE, new int[]{WATER_SAND_EDGE});
        Tile tileSandWaterSandCorner = new Tile("tileSandWaterSandCorner", "images/tilesets/08-sandWaterSandCorner.png", 1, SAND_WATER_EDGE, new int[]{WATER_SAND_EDGE}, WATER_SAND_EDGE, new int[]{SAND_WATER_EDGE}, SAND_EDGE, new int[]{SAND_EDGE}, SAND_EDGE, new int[]{SAND_EDGE});
        Tile tileSandWaterWaterCorner = new Tile("tileSandWaterWaterCorner", "images/tilesets/09-sandWaterWaterCorner.png", 1, SAND_WATER_EDGE, new int[]{WATER_SAND_EDGE}, WATER_EDGE, new int[]{WATER_EDGE}, WATER_EDGE, new int[]{WATER_EDGE}, WATER_SAND_EDGE, new int[]{SAND_WATER_EDGE});
        Tile tileSandGrassHalf = new Tile("tileSandGrassHalf", "images/tilesets/10-sandGrassHalf.png", 1, GRASS_EDGE, new int[]{GRASS_EDGE}, GRASS_SAND_EDGE, new int[]{SAND_GRASS_EDGE}, SAND_EDGE, new int[]{SAND_EDGE}, SAND_GRASS_EDGE, new int[]{GRASS_SAND_EDGE});
        Tile tileSandGrassSandCorner = new Tile("tileSandGrassSandCorner", "images/tilesets/11-sandGrassSandCorner.png", 1, SAND_GRASS_EDGE, new int[]{GRASS_SAND_EDGE}, GRASS_SAND_EDGE, new int[]{SAND_GRASS_EDGE}, SAND_EDGE, new int[]{SAND_EDGE}, SAND_EDGE, new int[]{SAND_EDGE});
        Tile tileSandGrassGrassCorner = new Tile("tileSandGrassGrassCorner", "images/tilesets/12-sandGrassGrassCorner.png", 1, SAND_GRASS_EDGE, new int[]{GRASS_SAND_EDGE}, GRASS_EDGE, new int[]{GRASS_EDGE}, GRASS_EDGE, new int[]{GRASS_EDGE}, GRASS_SAND_EDGE, new int[]{SAND_GRASS_EDGE});
        Tile tileSandGrassWater = new Tile("tileSandGrassWater", "images/tilesets/13-sandGrassWater.png", 1, SAND_GRASS_EDGE, new int[]{GRASS_SAND_EDGE}, GRASS_SAND_EDGE, new int[]{SAND_GRASS_EDGE}, SAND_WATER_EDGE, new int[]{WATER_SAND_EDGE}, WATER_SAND_EDGE, new int[]{SAND_WATER_EDGE});

        defaultTileSet.tileSheet.add(tileGrass);
        defaultTileSet.tileSheet.add(tileGrass2);
        defaultTileSet.tileSheet.add(tileWater);
        defaultTileSet.tileSheet.add(tileWater2);
        defaultTileSet.tileSheet.add(tileSand);
        defaultTileSet.tileSheet.add(tileWaterSandHalf);
        defaultTileSet.tileSheet.add(tileSandWaterSandCorner);
        defaultTileSet.tileSheet.add(tileSandWaterWaterCorner);
        defaultTileSet.tileSheet.add(tileSandGrassHalf);
        defaultTileSet.tileSheet.add(tileSandGrassSandCorner);
        defaultTileSet.tileSheet.add(tileSandGrassGrassCorner);
        defaultTileSet.tileSheet.add(tileSandGrassWater);

        defaultTileSet.generatedTileOptions = convertTileSheetToTileOptions(defaultTileSet.tileSheet);
        return defaultTileSet;
    }

    public static TileSet getEmptyTileSet() {
        TileSet emptyTileSet = new TileSet();
        emptyTileSet.setName("emptyTileSet");
        emptyTileSet.tileSheet = new ArrayList<>();
        Tile tileEmpty = new Tile("defaultTile", "images/tilesets/00-defaultTile.png", 1, DEFAULT_EDGE, new int[]{DEFAULT_EDGE}, DEFAULT_EDGE, new int[]{DEFAULT_EDGE}, DEFAULT_EDGE, new int[]{DEFAULT_EDGE}, DEFAULT_EDGE, new int[]{DEFAULT_EDGE});
        emptyTileSet.tileSheet.add(tileEmpty);
        emptyTileSet.generatedTileOptions = convertTileSheetToTileOptions(emptyTileSet.tileSheet);
        return emptyTileSet;
    }

    private static boolean areAllEdgesEqual(int top, int right, int bottom, int left) {
        return top == right && top == bottom && top == left;
    }
}
