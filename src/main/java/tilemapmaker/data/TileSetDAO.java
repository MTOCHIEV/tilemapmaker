package tilemapmaker.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TileSetDAO {

    @JsonProperty("tileSheet")
    private final List<TileDAO> tileSheet;
    @JsonProperty("name")
    private final String name;

    public TileSetDAO(List<TileDAO> tileSheet, String name) {
        this.tileSheet = tileSheet;
        this.name = name;
    }

    public TileSetDAO(String name) {
        this.name = name;
        this.tileSheet = new ArrayList<>();
    }

    public TileSetDAO(TileSet tileSet) {
        this.name = tileSet.getName();
        this.tileSheet = new ArrayList<>();
        for (Tile tile : tileSet.getTileSheet()) {
            TileDAO tileDAO = new TileDAO(tile);
            this.tileSheet.add(tileDAO);
        }
    }

    public static TileSetDAO getEmptyTileDAO() {
        TileSetDAO tileSetDAO = new TileSetDAO("emptyTileSet");
        return tileSetDAO;
    }

    public String getName() {
        return this.name + ".json";
    }
}
