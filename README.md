# TileMapMaker
![The interface](/src/main/resources/images/logo_avatar.png "Interface")

## About
TileMapMaker.org is a procedural tilemap generation website.

https://tilemapmaker.org/

![The interface](/src/main/resources/images/interface.png "Interface")

* TileMapMaker uses wave function collapse to generate random and arbitrarily sized 2D maps from a configurable tile sheet
* For more detailed information and instructions, see the ![docs](/src/main/resources/docs/TileMapMaker.pdf)